---
title: "Documentation du carrefour des innovations sociales"
type: index
---

Le carrefour des innovations sociales vise à rendre accessible, de manière fiable et mise à jour,
l’ensemble des innovations sociales déjà recensées en ligne.

Vous trouverez la documentation de conception et de développement de la maquette fonctionnelle réalisée par [Makina Corpus](http://makina-corpus.com/).

Documents disponibles :

* [Document de conception et d'architecture](01-architecture.md)
* [Document de développement et d'exploitation](02-exploitation.md)
* [Manuel d'utilisation](03-utilisateur.md)
