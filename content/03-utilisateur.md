---
title: "Manuel d'utilisation"
---

# Utilisation du moteur de recherche

Il se trouve sur cette page: http://mc-cget.surge.sh/

Quatre projets sont affichés sur la page d'accueil (pour le moment un choix arbitraire).

Pour rechercher des projets, cliquer sur le lien dans la navigation. 

## Filtres de recherche

* texte libre, dans le titre ou le résumé
* mots-clés (sélection multiple)
* région ou département (sélection multiple)
* sites contributeurs (sélection multiple)
* contient une vidéo

## Affichage

Les projets peuvent être affichés en vignette (grille) ou tableau.

## Carte

Les projets ayant une localisation renseignée sont affichés sur la carte. En cliquant sur un projet on affiche une popup sur son marker. La carte est recentrée sur la liste des projets.

## Suggestions d'amélioration

### Recherche

* Thématique : catégories / sous-catégorie
* Recherche croisée : thème et lieu

### Affichage

* pagination pour l'affichage en vignette
* Exporter les résultats
* Sélectionner les projets pour un éventuel export
* Visualiser les liens entre expériences

### Carte

* Rechercher par proximité géographique
* Préciser périmètre géographique de la recherche
* Filtrer les projets en fonction de la bbox de la carte
* Utiliser les markers cluster pour regrouper les projets 

# API et interface d'administration

L'interface d'administration est disponible ici :

http://social-connect-cget.herokuapp.com/

On peut accéder en public à la liste des contributeurs et le nombre de projets affichés.

## Utilisation de l'API

La [documentation de l'API](https://social-connect-cget.herokuapp.com/api/docs/) est disponible en ligne.

## Interface d'administration

La gestion des utilisateurs et des groupes est disponible par défaut dans django. Pour créer un utilisateur, cliquer sur "ajouter" dans la ligne "Utilisateurs" de la section "Authentification et autorisation".

Les types de contenus de l'application sont dans la section "Projets" :

* contributeurs
    * nom à afficher
    * nom du spider
    * site web
* mot-clés
    * label
    * value
* projets

La base a été remplie par l'API à l'aide de l'outil de scraping indépendant de l'application d'administration, et documenté dans la section exploitation.
