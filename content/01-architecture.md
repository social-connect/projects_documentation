---
title: "Conception et Architecture"
---

# Indexation des données

L'indexation (crawling) des données se fait avec [Scrapy](http://scrapy.readthedocs.io/), et [scrapyd](http://scrapyd.readthedocs.org/) pour faire une récupération régulière.

Les données indéxées par le module d'indexation sont stockées en base de données par l'intermédiaire de l'API. Un utilisateur créé dans l'application d'administration, associé à un token permet de se connecter à l'API pour pouvoir créer les données (voir la partie API).

Scrapyd est utilisé pour planifier une indexation régulière.

# Persistance des données

Le site d'administation et l'API sont réalisés avec [Django 1.11](https://docs.djangoproject.com/fr/1.11/) et [Django Rest Framework](http://django-rest-framework.org/), et les données sont stockées sur une base de données [PostgreSQL](http://postgresql.org/).

Plusieurs aspects ont motivé l'utilisation d'un framework (cadriciel) complet pour cette partie.

* Les contributeurs devront à terme pouvoir se connecter et accéder aux données qu'il partage, à des outils de dataviz, des outils de suivi des recherches sur ses contenus (tags les plus recherchés, etc.) : un module de connexion est donc nécessaire.

* Les évolutions futures doivent être prises en compte : fonctionnalités de mise en relation, collection d'initiatives, espace en ligne pour les communautés restreintes, système d'offre d'emploi, appel à expertise, demande de besoin… **Django** est un framework suffisamment modulable et complet pour permettre ces évolutions futures.

* La recherche géographique peut potentiellement nécessiter l'utilisation de l'extension PostGIS à la base de données PostgreSQL, et [Django intègre nativement cette extension](https://docs.djangoproject.com/fr/1.11/ref/contrib/gis/install/postgis/). 

## Schéma des données

Les données suivent le schéma suivant.

![Schéma de données](./social-projects-schema.png)

* Contributeur (Sourceur)
    * name
    * spider_name
    * url

* Project (projet d'innovation,initiative, action)
    * name
    * contributor → Contributor (ForeignKey)
    * keywords → Keyword (ManyToMany)
    * abstract
    * link
    * img
    * video_url
    * area (region, departement, commune, communauté de commune)
    * latitude
    * longitude
    * contact
    * project_owner
    * status
    * partner
    * economic_model
    * date

* Keyword (mot-clés, domaine ou thématique ?)
    * name
    * label

## Données futures

Selon les fonctionnalités futures attendues, il est possible d'enrichir les modèles présents et d'en ajouter de nouveaux.

* Contributeur
    * user
    * name
    * base_url
    * search_url
    * sourceur_type (pouvoirs public, acteurs privés, têtes de réseau, professionnels de l'accompagnement) -> modèle type ?
    * is_partenaire

Il pourrait être intéressant de créer un modèle `Contact` ou `Actor`, relier à `contact`, `project_holder` et `partner` et contenant des champs tels que `name`, `phone`, `email` ou encore `website`.

* Actor : sourceurs référents, porteurs de projet, "utilisateurs" des innovations
    * name
    * mail
    * url
    * phone

Il est ensuite possible de lier ce modèle au modèle `User` qui est fournit par django et permettre à des utilisateurs de se connecter sur la base pour compléter leurs informations.

Pour créer des grandes thématiques, une possibilité est d'ajouter au modèle `Project` un champs `domain` lié à `Keyword`, et d'ajouter au modèle `Keyword` un champ `keyword_parent` lié à lui-même.

Il pourrait y avoir plusieurs dates pour un même projet : date de publication de la fiche, date du projet, date d'ajout dans la base.

# API

## Architecture

L'API est réalisée à l'aide de [Django Rest Framework](http://django-rest-framework.org/) et située dans l'app `api` de l'application django.

## Données sérialisées

La [documentation en ligne](https://social-connect-cget.herokuapp.com/api/docs/) est générée automatiquement par l'outil [django-rest-swagger](https://django-rest-swagger.readthedocs.io/). Seules les méthodes GET sont visibles.

Comme le montre la documentation il est possible de faire des requêtes sur les projets avec des filtres comme `title` pour une recherche exact, ou `title_contains` pour une recherche d'un mot dans le titre.

Les filtres possibles sont décrits dans la documentation sur les filtres de [DjangoFilterBackend](http://www.django-rest-framework.org/api-guide/filtering/#djangofilterbackend).

## Droits d'accès et "token"

Par défaut toute l'API est en lecture seule, la méthode `GET` seule est disponible en mode non connecté.

Pour utiliser les méthodes `PUT` ou `DELETE` on utilise l'authentification par token. Lorsqu'un nouvel utilisateur est créé, un token est généré et visible dans l'administration du site : il pourra être utilisé par une application extérieure. C'est ce qui est utilisé pour envoyer les projets indexés par scrapy.

# Site web public

Le site web est réalisé avec le framework [Reactjs](http://reactjs.org/), et récupère les données via l'API. 

## Filtres de recherche

La recherche se fait sur les critères suivants :

* recherche libre sur titre et résumé
* par mot-clés
* par région et départements
* par contributeur
* contenant ou non des vidéos

## Données affichées

L'affichage se fait de deux façons : en grille et en tableau triable.

Les données affichées en grille :

* Titre, image, localisation, 3 mot-clés
* En plus : tous les mot-clés, lien vers la fiche, contributeur, contact, porteur de projet, lien vidéo

Les données affichées en tableau : titre, localisation et contributeur.

## Carte

La carte utilise la bibliothèque [Leaflet](http://leafletjs.com/), les données géographique de [OpenStreetMap](http://openstreetmap.org/) et le fond de carte Humanitarian.

Le titre est affiché avec une "popup" sur le point localisé.

## Pages statiques

Les pages utilise le routing de Reactjs

* Page d'accueil résumant le projet et affichant les derniers projets
* Rechercher des projets : page du moteur de recherche
* À propos : présentation du projet de carrefour de l'innovation sociale
* Contributeurs : liste des partenaires sourceurs alimentant les données

## Les composants ReactJS

* L'application globale se trouve dans `App.js`, qui contient les routes vers les différentes pages du site (voir ci-dessus la rubrique 'pages statiques') ainsi que le lien vers le composant permettant de faire la recherche des projets.

* Le composant `SearchProjectApp` est le composant parent de la partie dynamique du site. C'est lui qui contient le formulaire de recherche (`SearchForm/index.js`), et la zone de résultats, elle-même composée des projets listés et de la carte (Projects/index).

```
searchProjectApp
├── SearchForm/index
├── Projects/index
```

* `SearchForm` est lui-même le parent de plusieurs composants permettant d'effectuer les recherches. Voici un schéma de la structure :
```
SearchForm/index
├── SearchLibre/index (pour les recherches sur les titres et les résumés)
├── SearchByKeyword/index 
├── SearchRegion/index 
├── SearchDepartement/index 
├── SearchSourceur/index 
└── SearchMedia/index
```

* `Projects` est le conteneur des composants liés aux résultats de la recherche. La restitution des résultats se fait de trois manières possibles : vignette, table et carte.

```
Projects/index
├── ProjectList/index (résultats restitués en grille ou en tableau)
    ├── ProjectsViewTable/index (vue en tableau)
        ├── ProjectsTableHead (tri du tableau)
        └── ProjectsTableBody (résultats)
    └── ProjectsViewGrid/index (vue en grille)
        └── ProjectsItem/index (vue détail vignette)
└── MapComponent/index (carte avec markers des projets)
```

## Le lien avec l'API

Le lien avec l'API se fait avec une API javascript nommée `fetch` qui fournit une interface pour la récupération des données en construisant des requêtes et des réponses HTTP.
Ces requêtes se trouve au niveau du composant `SearchProjectApp`, via la méthode `getProjects()`.

# Déploiement

## Site d'administration

Il est déployé sur [heroku](https://dashboard.heroku.com/apps/social-connect-cget)

## Site du moteur de recherche

Il est déployé sur [Surge](http://surge.sh/)
