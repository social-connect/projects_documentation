# Documentation du projet Carrefour de l'innovation sociale

## Liste des documents

* [Document de conception et d'architecture](./content/01-architecture/index.md)
* Manuel d'installation et d'exploitation
    * [système d'indexation](./content/02-exploitation/01-crawling.md) (crawling)
    * [api et backoffice](./content/02-exploitation/02-api.md)
    * [moteur de recherche](./content/02-exploitation/03-website.md)
* [Guide utilisateur](./content/03-utilisateur/index.md)
* État des lieux de l'indexation des sites

## Contribuer à la documentation

La documentation est éditée au format markdown, et peut être exportée au format odt ou pdf (ou autre au choix) avec [pandoc](http://pandoc.org/), ou exportée sous forme de site web avec [Hugo](https://gohugo.io/).

Cloner le dépôt de documentation

    git clone git@gitlab.makina-corpus.net:cget/documentation.git

Exporter en odt avec pandoc

    pandoc content/01-architecture/index.md -o export/01-document-conception-architecture.odt
    pandoc -o export/01-document-conception-architecture.odt \
        content/02-exploitation/01-crawling.md \
        content/02-exploitation/02-api.md \
        content/02-exploitation/03-website.md \
