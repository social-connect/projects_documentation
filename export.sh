FORMAT=latex  # as understood by -t <format> in pandoc
FLAGS=--toc   # other flags for pandoc, --smart, etc
OUT=$OUT       # output extension
cd content
pandoc $FLAGS --standalone -o ../export/01-document-architecture.$OUT 01-architecture.md --reference-$OUT=$HOME/Modèles/makina.$OUT
pandoc $FLAGS --standalone -o ../export/02-document-developpement-exploitation.$OUT 02-exploitation.md --reference-$OUT=$HOME/Modèles/makina.$OUT
pandoc $FLAGS --standalone -o ../export/03-document-utilisateur.$OUT 03-utilisateur.md --reference-$OUT=$HOME/Modèles/makina.$OUT
